'use strict';

angular.module('filterApp', [])
    .controller('FilterCtrl', function() {
      this.inputText = "Ahoj světe ...";

      this.search = {
        status: 'open'
      };

      this.todos = [
        {status: "open", name: "TODO 03", owner: "Petr", duedate: 1422509857000},
        {status: "open", name: "TODO 09", owner: "Pavel", duedate: 1422309857000},
        {status: "open", name: "TODO 04", owner: "Petr", duedate: 1422609857000},
        {status: "open", name: "TODO 07", owner: "Pavel", duedate: 1422309857000},
        {status: "open", name: "TODO 05", owner: "Petr", duedate: 1422709857000},
        {status: "open", name: "TODO 02", owner: "Petr", duedate: 1422409857000},
        {status: "open", name: "TODO 01", owner: "Petr", duedate: 1422309857000},
        {status: "open", name: "TODO 08", owner: "Igor", duedate: 1422309857000},
        {status: "open", name: "TODO 06", owner: "Petr", duedate: 1422809857000},
        {status: "closed", name: "TODO 10", owner: "Petr", duedate: 1422309857000}
      ];
    })

    .filter("reverse", function() {
      return function(input, prefix) {
        input = input || "";
        prefix = prefix || "";

        var reversed = input.split("").reverse().join("");
        return prefix + reversed;
      };
    });